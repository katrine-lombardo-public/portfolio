import FormContact from "../components/FormContact";

const Contact = () => {
  return (
    <div className='columns-1 p-8'>
      <div className='flex justify-center'>
        <FormContact />
      </div>
    </div>
  );
};

export default Contact;
