import AboutMe from "../components/AboutMe";

const HomePage = () => {
  return (
    <div>
      <AboutMe />
    </div>
  );
};

export default HomePage;
